
package modelo;

public class Recibo {
    //ATRIBUTOS
    private String numeroRecibo;
    private String fecha;
    private String nombreCliente;
    private String domicilio;
    private int tipoRecibo;
    private float kilowatts;
    //CONSTRUCTOR VACIO
    public Recibo(){
        
    }
    //CONSTRUCTOR POR PARAMETROS

    public Recibo(String numeroRecibo, String fecha, String nombreCliente, String domicilio, int tipoRecibo, float kilowatts) {
        this.numeroRecibo = numeroRecibo;
        this.fecha = fecha;
        this.nombreCliente = nombreCliente;
        this.domicilio = domicilio;
        this.tipoRecibo = tipoRecibo;
        this.kilowatts = kilowatts;
    }

    //CONSTRUCTOR POR COPIA
    public Recibo(Recibo R) {
        this.tipoRecibo = R.tipoRecibo;
        this.kilowatts = R.kilowatts;
    }
    //METODOS GETTER & SETTER

    public String getNumeroRecibo() {
        return numeroRecibo;
    }

    public void setNumeroRecibo(String numeroRecibo) {
        this.numeroRecibo = numeroRecibo;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getNombreCliente() {
        return nombreCliente;
    }

    public void setNombreCliente(String nombreCliente) {
        this.nombreCliente = nombreCliente;
    }

    public String getDomicilio() {
        return domicilio;
    }

    public void setDomicilio(String domicilio) {
        this.domicilio = domicilio;
    }
    
    public int getTipoRecibo() {
        return tipoRecibo;
    }

    public void setTipoRecibo(int tipoRecibo) {
        this.tipoRecibo = tipoRecibo;
    }

    public float getKilowatts() {
        return kilowatts;
    }

    public void setKilowatts(float kilowatts) {
        this.kilowatts = kilowatts;
    }
    
    //AQUI SE MOSTRARAN LOS CALCULOS
    public float calcularSubtotal() {
       float costoKilowatt;
       if (tipoRecibo == 1) {
           costoKilowatt = 2.00f;
       } else if (tipoRecibo == 2) {
           costoKilowatt = 3.00f;
       } else if (tipoRecibo == 3) {
           costoKilowatt = 5.00f;
       } else {
           return 0; // Tipo de recibo inválido
       }

       return costoKilowatt * kilowatts;
   }

   public float calcularImpuesto() {
       float subtotal = calcularSubtotal();
       float impuesto = subtotal * 0.16f;
       return impuesto;
   }

   public float calcularTotal() {
       float subtotal = calcularSubtotal();
       float impuesto = calcularImpuesto();
       return subtotal + impuesto;
   }
}
