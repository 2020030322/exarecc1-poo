
package controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.concurrent.ExecutionException;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import modelo.Recibo;
import vista.dlgCFE;

public class Controlador implements ActionListener {
    private Recibo reci;
    private dlgCFE vista;
    private Controlador(Recibo reci, dlgCFE vista) {
        this.reci = reci;
        this.vista = vista;
        vista.btnCancelar.addActionListener(this);
        vista.btnCerrar.addActionListener(this);
        vista.btnGuardar.addActionListener(this);
        vista.btnLimpiar.addActionListener(this);
        vista.btnMostrar.addActionListener(this);
        vista.btnNuevo.addActionListener(this);
    }
    
    private void iniciarVista(){
        vista.setTitle("...::: Comicion Federal de Electricidad :::...");
        //Esta linea de codigo es para modifica el (Width,Heigth)
        vista.setSize(661, 480);//<<<<----^^^^^^^^^^^^^^^^^^^^^^^^
        vista.setLocationRelativeTo(null);
        vista.setVisible(true);        
    }
    //ESTE SE ACTIVO POR HACER EL CONTROLADOR ABSTRACTO
    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource()==vista.btnLimpiar){
            //LIMPIAR LO PRINCIPAL
            vista.txtNumRecibo.setText("");
            vista.txtFecha.setText("");
            vista.txtNombreCliente.setText("");
            vista.txtDomicilio.setText("");
            vista.cbTiPoServicio.getModel();
            vista.txtCostoKilowatts.setText("");
            vista.txtConsumidoKilowatts.setText("");
            //LIMPIAR LO QUE SE CALCULO
            vista.txtSubtotal.setText("");
            vista.txtImpuesto.setText("");
            vista.txtTotalPagar.setText("");
        }
        if(e.getSource()==vista.btnNuevo){
            //ACTIVARLOS DE NUEVO
            vista.txtNumRecibo.setEnabled(true);
            vista.txtFecha.setEnabled(true);
            vista.txtNombreCliente.setEnabled(true);
            vista.txtDomicilio.setEnabled(true);
            vista.cbTiPoServicio.setEnabled(true);
            vista.txtCostoKilowatts.setEnabled(true);
            vista.txtConsumidoKilowatts.setEnabled(true);
            //HACER QUE NO SE ACTIVEN
            vista.txtSubtotal.setEnabled(false);
            vista.txtImpuesto.setEnabled(false);
            vista.txtTotalPagar.setEnabled(false);
            //BOTONES PARA HABILITAR
            vista.btnGuardar.setEnabled(true);
            vista.btnMostrar.setEnabled(true);
        }
        if(e.getSource()==vista.btnGuardar){
            reci.setNumeroRecibo(vista.txtNumRecibo.getText());
            reci.setFecha(vista.txtFecha.getText());
            reci.setNombreCliente(vista.txtNombreCliente.getText());
            reci.setDomicilio(vista.txtDomicilio.getText());
            reci.setTipoRecibo(vista.cbTiPoServicio.getItemCount());
            try{
                reci.setKilowatts(Float.parseFloat(vista.txtConsumidoKilowatts.getText()));
            //}catch(ExecutionException ex){
                //JOptionPane.showMessageDialog(vista, "Surgio el siguiente error : " + ex.getMessage());
            JOptionPane.showMessageDialog(vista,"Se agrego Exitosamente"); 
            }catch(Exception ex){
                JOptionPane.showMessageDialog(vista, "Surgio el siguiente error : " + ex.getMessage());                
            }
            vista.txtSubtotal.setText(String.valueOf(reci.calcularSubtotal()));
            vista.txtImpuesto.setText(String.valueOf(reci.calcularImpuesto()));
            vista.txtTotalPagar.setText(String.valueOf(reci.calcularTotal()));
        }
        
        if(e.getSource()==vista.btnMostrar){
           vista.txtNumRecibo.setText(reci.getNumeroRecibo());
           vista.txtFecha.setText(reci.getFecha());
           vista.txtNombreCliente.setText(reci.getNombreCliente());
           vista.txtDomicilio.setText(reci.getDomicilio());
           //vista.
           //vista.txtCostoKilowatts.setText(reci.getKilowatts());
           //vista.txtConsumidoKilowatts.setText(reci.getKilowatts());
           vista.txtSubtotal.setText(String.valueOf(reci.calcularSubtotal()));
           vista.txtImpuesto.setText(String.valueOf(reci.calcularImpuesto()));
           vista.txtTotalPagar.setText(String.valueOf(reci.calcularTotal()));
        }
        
        if(e.getSource()==vista.btnCerrar){
                int option=JOptionPane.showConfirmDialog(vista,"Seguro que quieres salir",
                        "Decide",JOptionPane.YES_NO_OPTION);
                if(option==JOptionPane.YES_NO_OPTION){
                    vista.dispose();
                    System.exit(0);
                }
        }
        if(e.getSource()==vista.btnCancelar){
            //LIMPIAR Y DESHABILITAR LOS TXT
            vista.txtNumRecibo.setText("");
            vista.txtFecha.setText("");
            vista.txtNombreCliente.setText("");
            vista.txtDomicilio.setText("");
            vista.txtCostoKilowatts.setText("");
            vista.txtConsumidoKilowatts.setText("");
            vista.txtNumRecibo.setEnabled(false);
            vista.txtFecha.setEnabled(false);
            vista.txtNombreCliente.setEnabled(false);
            vista.txtDomicilio.setEnabled(false);
            vista.txtCostoKilowatts.setEnabled(false);
            vista.txtConsumidoKilowatts.setEnabled(false);
            //DESHABILITAR LOS BOTONES
            vista.btnMostrar.setEnabled(false);
            vista.btnGuardar.setEnabled(false);
            
        }        
    }
    public static void main(String[] args) {
        // TODO code application logic here
        Recibo reci = new Recibo();
        dlgCFE vista = new dlgCFE(new JFrame(),true);
        
        Controlador control = new Controlador(reci, vista);
        control.iniciarVista();
        
    }
}
    
